var express = require('express'), cors = require('cors');
var app = express();//se incializa un servidor web
app.use(express.json());
app.use(cors());
app.listen(3000, () => console.log("corriendo el serve el puerto 3000"));
var ciudades = ['Guatemala', 'Totonicapan', 'Momostenango', 'Quetzaltenango'];
app.get("/ciudades", (req, res, next) => res.json(ciudades.filter((c) => c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1)));
//app.get("/url",(req,res, next)=>res.json(["Guatemala","Totonicapan","XEla","Momos"]));
var misDestinos = [];
app.get("/my", (req, res, next) => res.json(misDestinos));
app.post("/my", (req, res, next) => {
    console.log(req.body);
    //misDestinos=req.body;
    misDestinos.push(req.body.nuevo);
    res.json(misDestinos);
});
//vamos agregar un nuevo web service que permtie traducir el idionma aqui se instal el framewoerk web servie de traducciones
app.get("/api/translation/", (req, res, next) => res.json([
    {lang: req.query.lang, key: 'HOLA', value: 'HOLA' + req.query.lang}
]));