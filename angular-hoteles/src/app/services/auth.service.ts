import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }
  login(user: string, password: string): boolean {
    if (user === "admin" && password === "123") {
      localStorage.setItem("username",user);//objeto de html5 localStorage que almacena informaci[on een el navegador]
      return true;
    }
    return false;
  }
  logout():any{
    localStorage.removeItem("username");
  }
  getUser():any{
    return localStorage.getItem("username");
  }
  isLoggenIn():boolean{
    return this.getUser()!==null;
  }
}
