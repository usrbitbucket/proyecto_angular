import { forwardRef, Inject, Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppConfig, AppState, APP_CONFIG, db } from '../app.module';
import { DestinoViaje } from './destino-viaje.model';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destinos-viajes-state.model';
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest, HttpResponse } from "@angular/common/http"; //se agrega un import de http a nuestro modulo

@Injectable()
export class DestinosApiClient {
	destinos: DestinoViaje[] = [];
	//constructor( private store: Store<AppState>) {
	//}
	constructor(private store: Store<AppState>,
		@Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig, private http: HttpClient) { //se usal el httpClietn porque permite hacer llamadas al web services
		//this.destinos = [];
		this.store
			.select(state => state.destinos)
			.subscribe((data) => {
				console.log('destino sub store');
				console.log(data);
				this.destinos = data.items;
			});
		this.store
			.subscribe((data) => {
				console.log('all store');
				console.log(data);
			});
	}

	//add(d: DestinoViaje){
	//	this.store.dispatch(new NuevoDestinoAction(d));
	//}
	add(d: DestinoViaje) {
		const header: HttpHeaders = new HttpHeaders({ 'X-API-TOKEN': 'token-seguridad' });
		const req = new HttpRequest('POST', this.config.apiEndpoint + '/my',{ nuevo: d.nombre }, {headers: header });
		this.http.request(req).subscribe((data:HttpResponse<{}>)=>{
			if (data.status===200) {
				this.store.dispatch(new NuevoDestinoAction(d));	
				const myDb=db;
				myDb.destinos.add(d);
				console.log('todos los destinos de la bd');
				myDb.destinos.toArray().then(destinos=>console.log(destinos));	
			}
		});		
	}
	elegir(d: DestinoViaje) {
		this.store.dispatch(new ElegidoFavoritoAction(d));
	}

	getAll(): DestinoViaje[] {
		return this.destinos;
	}
	getById(id: String): DestinoViaje {
		return this.destinos.filter(function (d) {
			return d.id.toString() === id;
		})[0];
	}
}